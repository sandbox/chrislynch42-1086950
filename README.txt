$Id$

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Frequently Asked Questions (FAQ)
 * Known Issues
 * How Can You Contribute?


INTRODUCTION
------------

Current Maintainer: Chris Lynch <http://drupal.org/user/42169>
Project Page: http://drupal.org/sandbox/chrislynch42/1086950

This module allows you to track companies.  Anything from
book publishers to automobile part manufacturers.  The module
is designed to be easily used by other modules.  If you 
have an idea on how the module could be changed to be more useful
please contact the maintainer.


INSTALLATION
------------

See http://drupal.org/getting-started/install-contrib for instructions on
how to install or update Drupal modules.


FREQUENTLY ASKED QUESTIONS
--------------------------

There are no frequently asked questions at this time.


KNOWN ISSUES
------------

There are no known issues at this time.

To report new bug reports, feature requests, and support requests, visit
http://drupal.org/project/issues/1086950.
