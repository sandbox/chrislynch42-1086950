<?php


/**
 * @file
 * Company module form include
 *
 */

/**
 * Function to build a form definition array.  Placed in a seperate method
 * so that other modules can call this function to build a form and not repeat
 * code.
 * @param object $node Node object being edited.
 * @return array Form definition
 */
function company_make_form(&$node) {
  $breadcrumb1 = company_get_breadcrumb();
  $breadcrumb2 = array(l(t($node->title) , 'node/' . $node->nid));
  $breadcrumb3 = array_merge($breadcrumb1 , $breadcrumb2);

  drupal_set_breadcrumb($breadcrumb3);

  $form['company_id'] = array(
    '#type' => 'hidden' ,
    '#default_value' => $node->company_id ,
    '#weight' => -20
  );

  $form['company'] = array(
    '#type' => 'fieldset' ,
    '#title' => t('Company') ,
    '#weight' => -8 ,
    '#collapsible' => FALSE ,
    '#collapsed' => FALSE ,
  );

  $form['company']['title'] = array(
    '#type' => 'textfield' ,
    '#title' => t('Name') ,
    '#required' => TRUE ,
    '#default_value' => $node->title ,
    '#weight' => -50
  );


  $form['company']['body_filter']['body'] = array(
      '#type' => 'textarea' ,
      '#title' => t('Description') ,
      '#default_value' => $node->body ,
      '#required' => FALSE ,
      '#weight' => -49
  );

  $form['status'] = array(
    '#type' => 'hidden' ,
    '#default_value' => '1'
  );

  $form['promote'] = array(
    '#type' => 'hidden' ,
    '#default_value' => '1'
  );

  $form['name'] = array(
    '#type' => 'hidden' ,
    '#default_value' => $user->name
  );

  $form['format'] = array(
    '#type' => 'hidden' ,
    '#default_value' => '1'
  );

  $form['theletter'] = array(
    '#type' => 'hidden' ,
    '#default_value' => drupal_strtoupper(drupal_substr($node->title , 0 , 1))
  );

  $form['#submit']=array('company_form_submit');


  return $form;
}

/**
 * Function to pull breadcrumb information.  This information may be set
 * by this module or other modules.
 * 
 * @return array breadcrumb definition
 */
function company_get_breadcrumb() {
  $breadcrumb = "";
  if($_SESSION['company_breadcrumb']) {
    $breadcrumb  = $_SESSION['company_breadcrumb'];
  } else {
    $breadcrumb = array(l(t('Home') , NULL));
  }
  return $breadcrumb;
}