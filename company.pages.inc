<?php


/**
 * @file
 * Company module page include
 *
 */

/**
 * A list of all companies.  Can be limited by to those beginning with a
 * specific letter.
 * @global object $user Information about the person logged in.
 * @param string $theletter Letter to limit results with.
 * @return string Rendered HTML.
 */
function company_page_list($theletter=NULL) {
  $_SESSION['company_breadcrumb']= array(l(t('Home') , NULL) , l(t('Companies') , 'company'));

  $output = '';
  $items = array();

  if (user_access('create company entry')) {
    $items[] = l(t('Create new company entry.') , "node/add/company");
  }

  $output = theme('item_list' , $items);

  $result = db_query("SELECT DISTINCT substr(replace(upper(n.title) , 'THE' , '') , 1 , 1) theLetter FROM {node} n WHERE n.type = 'company' AND n.status = 1 ORDER BY theLetter");
  $output .= l('[' . t('ALL') . ']' , 'company');
  while ($node = db_fetch_object($result)) {
    $output .= l('[' . $node->theLetter . ']' , 'company/'. $node->theLetter);
    $has_posts = TRUE;
  }

  if ($theletter) {
    $result = db_query(db_rewrite_sql("SELECT n.nid FROM {node} n WHERE n.type = 'company' AND n.status = 1 AND substr(replace(upper(n.title) , 'THE' , '') , 1 , 1)='%s' ORDER BY n.title, n.created DESC") , $theletter);
  }
  else {
    $result = db_query(db_rewrite_sql("SELECT n.nid FROM {node} n WHERE n.type = 'company' AND n.status = 1 ORDER BY n.title , n.created DESC"));
  }
  $has_posts = FALSE;

  while ($record = db_fetch_object($result)) {
    $loaded_node = node_load($record->nid);
    $output .= node_view($loaded_node , TRUE , FALSE , TRUE);
    $has_posts = TRUE;
  }

  if ($has_posts) {
    //nothing
  }
  else {
    drupal_set_message(t('No company entries have been created.'));
  }
  return $output;
}

